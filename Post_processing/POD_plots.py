import os
import matplotlib.pyplot as plt
import numpy as np
import matplotlib
from pathlib import Path
from scipy.signal import medfilt2d
import scipy

"""
   This script plots temporal and modal POD plots.
"""

# Defining the experiment name
experiment_name = 'PIV_900rpm_0.3BEP'

# Defining POD folder
# For the POD decomposition with the 2500 transient fields:
POD_folder = Path('../POD_results/')

# For the POD decomposition with the 200 transient fields, generated from
# the samples and script from ../Snapshot_PIV, uncomment the line below:
# POD_folder = Path('../Snapshot_PIV/POD_output/')

# Defining .npz POD file
POD_filename = Path(POD_folder, f'{experiment_name}_POD_reconstruct.npz')

# Loading .npz POD file
POD_file = np.load(POD_filename)

# Create output folder
output_folder = Path('output', experiment_name)
output_folder.mkdir(parents=True, exist_ok=True)

# Retrieving data from the POD file
X     = POD_file['X']
Y     = POD_file['Y']
N_x   = POD_file['N_x']
N_y   = POD_file['N_y']
A     = POD_file['A']
PHI_T = POD_file['PHI_T']
U_avg = POD_file['U_avg']
V_avg = POD_file['V_avg']
LAM_s = POD_file['LAM_s']
MASKS = POD_file['MASKS']

# Number of reconstructed modes
N_reconstruct = LAM_s.shape[0]

# Plotting energectic modes
plt.plot(100.0 * LAM_s[:50] / np.sum(LAM_s))
plt.xlabel("k-th mode")
plt.ylabel("$\lambda_k$ / $\sum_{k=1}^{k=N} \lambda_k$")
plt.ylim(0.0,30.0)
plt.tight_layout()
fig_label = 'energetic_modes'
plt.savefig(Path(output_folder, f'{experiment_name}_{fig_label}.png'))
plt.close()

# Plotting cumulative energy of the modes
CUMSUM = np.cumsum(LAM_s)
plt.plot(CUMSUM / np.sum(LAM_s))
plt.xlabel("k-th mode")
plt.ylabel("$\sum_{k=1}^{k=k} \lambda_k$ /  $\sum_{k=1}^{k=N} \lambda_k$")
fig_label = 'cumulative_energy'
plt.savefig(Path(output_folder, f'{experiment_name}_{fig_label}.png'))
plt.close()

# Total number of modes
#         or
# Total number of time acquistions
N_modes = A.shape[0]

A_full = A.copy()

# Delta time in [s]
TR_PIV_RESOLUTION = 700.0 # [Hz]
dt = 1. / TR_PIV_RESOLUTION

# FFT power plots - 1st and 2nd modes
mode_0 = A_full[:,0]
n = mode_0.shape[0]
yf = scipy.fft.fft(mode_0)
xf = scipy.fft.fftfreq(n, (dt))[:n//2]
plt.plot(xf, 2.0/n * np.abs(yf[0:n//2]), label = '1st mode')

mode_1 = A_full[:,1]
n = mode_1.shape[0]
yf = scipy.fft.fft(mode_1)
xf = scipy.fft.fftfreq(n, (dt))[:n//2]
plt.plot(xf, 2.0/n * np.abs(yf[0:n//2]), label = '2nd mode')

plt.xlim(0,50)
plt.legend()
plt.xlabel("Freq [Hz]")
plt.ylabel("Power")
fig_label = 'FFT_power_modes_1_and_2'
plt.savefig(Path(output_folder, f'{experiment_name}_{fig_label}.png'))
plt.close()


# FFT power plots - 3rd and 4th modes
mode_2 = A_full[:,2]
n = mode_2.shape[0]
yf = scipy.fft.fft(mode_2)
xf = scipy.fft.fftfreq(n, (dt))[:n//2]
plt.plot(xf, 2.0/n * np.abs(yf[0:n//2]), label = '3rd mode')

mode_3 = A_full[:,3]
n = mode_3.shape[0]
yf = scipy.fft.fft(mode_3)
xf = scipy.fft.fftfreq(n, (dt))[:n//2]
plt.plot(xf, 2.0/n * np.abs(yf[0:n//2]), label = '4th mode')

plt.xlim(0,50)
plt.legend()
plt.xlabel("Freq [Hz]")
plt.ylabel("Power")
fig_label = 'FFT_power_modes_3_and_4'
plt.savefig(Path(output_folder, f'{experiment_name}_{fig_label}.png'))
plt.close()

# Plotting temporal coefficients over time - 1st and 2nd modes
N_samples = len(LAM_s)
plt.plot(1000.0 * (1.0/700.0) * np.arange(N_samples), mode_0[:N_samples], label = '1st mode')
plt.plot(1000.0 * (1.0/700.0) * np.arange(N_samples), mode_1[:N_samples], label = '2nd mode')
plt.legend()
plt.xlabel('Time [ms]')
plt.ylabel('$A_n$(t)')
plt.xlim(0,700)
fig_label = 'tempor_coeffs_modes_1_and_2'
plt.savefig(Path(output_folder, f'{experiment_name}_{fig_label}.png'))
plt.close()

# Plotting temporal coefficients over time - 1st and 2nd modes
N_samples = len(LAM_s)
plt.plot(1000.0 * (1.0/700.0) * np.arange(N_samples), mode_2[:N_samples], label = '3rd mode')
plt.plot(1000.0 * (1.0/700.0) * np.arange(N_samples), mode_3[:N_samples], label = '4th mode')
plt.legend()
plt.xlabel('Time [ms]')
plt.ylabel('$A_n$(t)')
plt.xlim(0,700)
fig_label = 'tempor_coeffs_modes_3_and_4'
plt.savefig(Path(output_folder, f'{experiment_name}_{fig_label}.png'))
plt.close()

# Phase diagrams - 1st vs. 2nd mode
a_0 = A_full[:,0]
a_1 = A_full[:,1]
plt.plot(a_0, a_1)
plt.xlabel('$a_1(t)$')
plt.ylabel('$a_2(t)$')
ax = plt.gca()
ax.axis('equal')
fig_label = 'phase_diagram_modes_1_vs_2'
plt.savefig(Path(output_folder, f'{experiment_name}_{fig_label}.png'))
plt.close()
