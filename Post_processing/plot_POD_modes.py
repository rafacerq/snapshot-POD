import sys
import os
import matplotlib.pyplot as plt
import numpy as np
import config as cfg
import matplotlib
from pathlib import Path
from scipy.signal import medfilt2d

from aux_functions.return_streamlines import return_streamlines

"""
    This script plots the POD spatial modes from a given
    experimental condition.
"""

# Defining matplotlib default colormap
plt.rcParams['image.cmap'] = 'jet'

# Defining the experiment name
experiment_name = 'PIV_600rpm_1.5BEP'

# Defining POD folder
# For the POD decomposition with the 2500 transient fields:
POD_folder = Path('../POD_results/')

# For the POD decomposition with the 200 transient fields, generated from
# the samples and script from ../Snapshot_PIV, uncomment the line below:
# POD_folder = Path('../Snapshot_PIV/POD_output/')

# Defining .npz POD file
POD_filename = Path(POD_folder, f'{experiment_name}_POD_reconstruct.npz')

# Loading .npz POD file
POD_file = np.load(POD_filename)

# Create output folder
output_folder = Path('output', experiment_name, 'POD_modes')
output_folder.mkdir(parents=True, exist_ok=True)

# Retrieving data from the POD file
X     = POD_file['X']
Y     = POD_file['Y']
N_x   = POD_file['N_x']
N_y   = POD_file['N_y']
A     = POD_file['A']
PHI_T = POD_file['PHI_T']
U_avg = POD_file['U_avg']
V_avg = POD_file['V_avg']
MASKS = POD_file['MASKS']
center_mm = POD_file['center_mm']

# Looping over the 1st to 4th mode
for mode in range(4):
    mode_U = PHI_T[mode, :N_x * N_y]
    mode_V = PHI_T[mode, N_x * N_y:]

    mode_U = np.reshape(mode_U, (N_x, N_y))
    mode_V = np.reshape(mode_V, (N_x, N_y))

    # Applying a medfilt2d filter for prettier plots
    # NOTE: This is only used for visualization purpouses
    mode_U = medfilt2d(mode_U, (3,3))
    mode_V = medfilt2d(mode_V, (3,3))

    # Computing spatial component magnitude
    mode_MAG = np.sqrt(mode_U**2.0 + mode_V**2.0)

    # Applying masks
    MASK = MASKS[0]
    mode_U[MASK > 10] = np.nan
    mode_V[MASK > 10] = np.nan
    mode_MAG[MASK > 10] = np.nan

    U_avg[MASK > 150] = np.nan
    V_avg[MASK > 150] = np.nan
    mode_MAG_avg = np.sqrt(U_avg**2.0 + V_avg**2.0)

    # Normalzing fields
    mode_MAG /= np.nanmax(mode_MAG)

    # Reading the impeller mask from a 'dummy' .npz file
    npz_folder = POD_filename.name.split("_POD")[0]
    npz_filename = Path(cfg.EXPERIMENTAL_FOLDER, npz_folder, f'results_complete_{0:06d}.npz')
    npz_file = np.load(npz_filename)
    STREAM_mask = npz_file['img_mask']

    # Returning streamline
    x_stream, y_stream = return_streamlines(X, Y, X, Y, mode_U, mode_V, STREAM_mask, dt=1.0e-4, N_iters=10000, N_random=2200)
    x_stream -= center_mm[0]
    y_stream -= center_mm[1]

    # Plot with streamlines
    ax = plt.gca()
    pmesh = ax.pcolormesh(X - center_mm[0], Y - center_mm[1], mode_MAG)
    cbar = plt.colorbar(pmesh,ax=ax, shrink=0.70)
    plt.plot(x_stream, y_stream, color='black', linewidth=0.4, alpha=0.8)
    ax.set_xlabel('$x$ [mm]')
    ax.set_ylabel('$y$ [mm]')
    ax.set_aspect('equal','box')
    ax.axis('off')
    ax.set_xlim(-60, 60)
    ax.set_ylim(-60, 60)
    plt.title(f'mode #{mode:02d}')
    plt.tight_layout()
    fig_label = f'POD_spatial_mode_{mode:02d}'
    plt.savefig(Path(output_folder, f'{experiment_name}_{fig_label}.png'))
    plt.close()
