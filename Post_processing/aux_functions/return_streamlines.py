import numpy as np
import matplotlib.pyplot as plt
import os
import h5py
import cv2
from mpi4py import MPI
import time
from scipy.signal import medfilt2d
import sys

from aux_functions.interpolate_values import interpolate_values

def return_streamlines(X_PIV, Y_PIV, X_PIV_imp, Y_PIV_imp,
                      U_PIV, V_PIV, mask_PIV,
                      dt = 1.0e-3, N_iters=10000, N_random=200):

    """
        This function return streamlines from the POD-reconstructed PIV fields
    """

    # Copying the PIV values
    X     = X_PIV.copy()     ; Y     = Y_PIV.copy()
    X_imp = X_PIV_imp.copy() ; Y_imp = Y_PIV_imp.copy()
    U     = U_PIV.copy()     ; V     = V_PIV.copy()
    mask  = mask_PIV.copy()

    # dX and dY to correct
    X_orig = X.copy()
    Y_orig = Y.copy()

    # Changing origin points
    X -= X.min()
    Y -= Y.min()

    # dX and dY to correct
    dX_corr = X_orig[0,0] - X[0,0]
    dY_corr = Y_orig[0,0] - Y[0,0]

    # Interpolating values
    interpol_level = 4.0
    X_i, Y_i, U_i          = interpolate_values(X, Y, U, mask, interpol_level)
    _  ,   _, V_i          = interpolate_values(X, Y, V, mask, interpol_level)
    X_imp_i, Y_imp_i, _    = interpolate_values(X_imp, Y_imp, V, mask, interpol_level)

    # Mask to use with interpolated values
    img_mask_i = cv2.resize(mask, (X_i.shape[1], X_i.shape[0]))
    geom_flag = img_mask_i > 10

    # Calculating radius
    R = np.sqrt(X_imp_i**2.0 + Y_imp_i**2.0)

    # From mm to m
    X_i     /= 1000.0;     Y_i /= 1000.0
    X_imp_i /= 1000.0; Y_imp_i /= 1000.0

    # Calculating delta_X and delta_Y
    dx = (X_i[0,1] - X_i[0,0])
    dy = (Y_i[1,0] - Y_i[0,0])

    # Applying mask to U and V fields
    U_i[geom_flag] = np.nan
    V_i[geom_flag] = np.nan

    # From nan to 0.0
    U_i[np.isnan(U_i)] = 0.0
    V_i[np.isnan(V_i)] = 0.0

    # Raveling the variables
    X_ravel = X_i.ravel()
    Y_ravel = Y_i.ravel()
    mask_ravel  = img_mask_i.ravel()

    # Generating random maps
    N_IWs = (X_i.shape[0] - 1) * (X_i.shape[1] - 1)
    IJ_random = np.linspace(0,N_IWs,num=N_IWs, dtype=int)
    np.random.shuffle(IJ_random)

    # Initializing variable
    x_s_0  = np.zeros((N_random), dtype=float)
    y_s_0  = np.zeros((N_random), dtype=float)

    # Randomly picking the seed points
    idx = 0
    for ij_random in IJ_random:
        if mask_ravel[ij_random] == 0:
            if idx > (N_random-1):
                continue
            x_s_0[idx] = X_ravel[ij_random]
            y_s_0[idx] = Y_ravel[ij_random]
            idx += 1

    # Initializing path lines
    stream_f = np.zeros((N_iters,len(x_s_0), 2), dtype=float) # forward in time
    stream_b = np.zeros((N_iters,len(x_s_0), 2), dtype=float) # backward in time

    # Advecting it forward on time
    points = (np.ravel(X_i), np.ravel(Y_i))
    for k in range(N_iters):
        if k == 0:
            x_s = x_s_0
            y_s = y_s_0

        stream_f[k,:,0] = x_s
        stream_f[k,:,1] = y_s

        i_s = x_s / dx
        j_s = y_s / dy

        i_s = i_s.astype(int)
        j_s = j_s.astype(int)

        x_s_new = U_i[j_s,i_s] * dt + x_s
        y_s_new = V_i[j_s,i_s] * dt + y_s
        x_s = x_s_new
        y_s = y_s_new

    # Advecting it backward on time
    for k in range(N_iters):
        if k == 0:
            x_s = x_s_0
            y_s = y_s_0

        stream_b[k,:,0] = x_s
        stream_b[k,:,1] = y_s

        i_s = x_s / dx
        j_s = y_s / dy

        i_s = i_s.astype(int)
        j_s = j_s.astype(int)

        x_s_new = U_i[j_s,i_s] * -1.0 * dt + x_s
        y_s_new = V_i[j_s,i_s] * -1.0 * dt + y_s
        x_s = x_s_new
        y_s = y_s_new

    # Flipping the backward-time paths
    stream_b = np.flipud(stream_b)

    # Concatenating into a single stream
    stream = np.concatenate((stream_b,stream_f))

    # Splitting into x and y paths
    x_stream = stream[:,:,0]
    y_stream = stream[:,:,1]

    # From m to mm
    x_stream *= 1000.0
    y_stream *= 1000.0

    # Correcting the (x,y) streamline values
    x_stream += dX_corr
    y_stream += dY_corr

    return x_stream, y_stream
