import numpy as np
from scipy.interpolate import griddata

def interpolate_values(X, Y, field, img_mask, interpolation_level=4):
    """
        This function interpolate the field values into a finer
        mesh point. Useful to generate "pretty/smooth" results
    """

    # Defining the new X and Y interpolated field
    grid_x = np.linspace(np.min(X), np.max(X), num=int(interpolation_level * X.shape[1]), endpoint=True)
    grid_y = np.linspace(np.min(Y), np.max(Y), num=int(interpolation_level * Y.shape[0]), endpoint=True)
    X_i, Y_i = np.meshgrid(grid_x, grid_y)

    # Original coordinates as [N,2] list, following the function requirements
    points = (np.ravel(X), np.ravel(Y))

    # Applying a linear interpolation
    interpolated_field = griddata(points, np.ravel(field), (X_i, Y_i), method='linear', rescale=False)

    return X_i, Y_i, interpolated_field
