import sys
import os
import matplotlib.pyplot as plt
import numpy as np
import config as cfg
import matplotlib
from pathlib import Path
from scipy.signal import medfilt2d
import ffmpeg

"""
    This script reconstruct the traniet PIV fields from the POD
    reconstruction .npz file.
"""

# Defining matplotlib default colormap and
# dark background
plt.rcParams['image.cmap'] = 'jet'
plt.style.use('dark_background')

# Number of reconstructed modes
N_reconstruct = 10

# Number of PIV timesteps
N_timesteps = 100

# Defining the experiment name
experiment_name = 'PIV_300rpm_0.3BEP'

# Defining POD folder
# For the POD decomposition with the 2500 transient fields:
POD_folder = Path('../POD_results/')

# For the POD decomposition with the 200 transient fields, generated from
# the samples and script from ../Snapshot_PIV, uncomment the line below:
# POD_folder = Path('../Snapshot_PIV/POD_output/')

# Defining .npz POD file
POD_filename = Path(POD_folder, f'{experiment_name}_POD_reconstruct.npz')

# Loading .npz POD file
POD_file = np.load(POD_filename)

# Create output folder
output_folder = Path('output', experiment_name, f'POD_reconstruct_{N_reconstruct:04d}_modes')
output_folder.mkdir(parents=True, exist_ok=True)

# Retrieving data from the POD file
X     = POD_file['X']
Y     = POD_file['Y']
N_x   = POD_file['N_x']
N_y   = POD_file['N_y']
A     = POD_file['A']
PHI_T = POD_file['PHI_T']
U_avg = POD_file['U_avg']
V_avg = POD_file['V_avg']
MASKS = POD_file['MASKS']
center_mm = POD_file['center_mm']

# Total number of modes
#         or
# Total number of time acquistions
A[:,N_reconstruct:] = 0.0

# Delta time in [s]
TR_PIV_RESOLUTION = 700.0 # [Hz]
dt = 1. / TR_PIV_RESOLUTION

# Reconstructing the velocity field
U_reconstruct = np.matmul(A, PHI_T, dtype=np.float64)

# Looping over the transient PIV fields
for k in range(0, N_timesteps, 1):

    print(f"Creating figures {k:02d}")

    # Time in ms
    t = 1000.0 * k * dt

    # Retrieving the instanteneous recontruction
    U_k = U_reconstruct[k, :N_x * N_y]
    V_k = U_reconstruct[k, N_x * N_y:]

    # Reshaping to N_x x N_y size
    U_k = np.reshape(U_k, (N_x, N_y))
    V_k = np.reshape(V_k, (N_x, N_y))

    # Summing average velocity field
    U_k += U_avg
    V_k += V_avg

    # Applying a 3x3 median filter
    U_k[np.isnan(U_k)] = 0.0
    V_k[np.isnan(V_k)] = 0.0
    U_k = medfilt2d(U_k, kernel_size=[3,3])
    V_k = medfilt2d(V_k, kernel_size=[3,3])

    # Applying masks
    MASK = MASKS[k]
    U_k[MASK > 10] = np.nan
    V_k[MASK > 10] = np.nan

    # Generate instanteneous plots
    U_avg_mag = np.sqrt(U_avg**2.0 + V_avg**2.0)
    U_k_mag = np.sqrt(U_k**2.0 + V_k**2.0)
    ax = plt.gca()
    quiv = ax.quiver(X - center_mm[0], Y - center_mm[1],
                     U_k, V_k, U_k_mag,
                     units='xy', scale_units=None,
                     pivot='mid', scale=0.14, clim=(0, np.nanmax(U_avg_mag)))
    ax.set_xlabel('$x$ [mm]')
    ax.set_ylabel('$y$ [mm]')
    ax.set_aspect('equal','box')
    ax.axis('off')
    ax.set_xlim(-60, 60)
    ax.set_ylim(-60, 60)
    if N_reconstruct == 1:
        title = f'{N_reconstruct:02d} mode'
    else:
        title = f'{N_reconstruct:02d} modes'
    plt.title(title)
    plt.tight_layout()
    cbar = plt.colorbar(quiv, shrink=0.7)
    cbar.ax.set_title('$|U|$ [m/s]')
    fig_label = f'timestep_{k:06d}.png'
    plt.text(-80, -50.0, f't = {t:.02f} ms')
    plt.savefig(Path(output_folder, fig_label), dpi=200)
    plt.close()

# Generating videos
output_video_filename = Path(output_folder, f'{experiment_name}_modes_{N_reconstruct:04d}.mp4')
output_options = {
    'crf': 20,
    'preset': 'slower',
    'movflags': 'faststart',
    'pix_fmt': 'yuv420p'
}
(
    ffmpeg
    .input('%s/*.png' % output_folder.resolve(), pattern_type='glob', framerate=15)
    .output(
        str(output_video_filename.resolve()),
        **output_options
    )
    .run(overwrite_output=True)
)
