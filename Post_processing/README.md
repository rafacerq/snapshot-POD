# Post_processing
This folder contains the post-processing scripts that can generate most of the POD-related plots available at XXXXXXXXXXXXXXXXX.
  * `POD_plots.py`: This script plots POD-related results presented in the manuscript;
  * `plot_POD_modes.py`: This script plots the spatial POD modes for each of the experimental conditions presented in the manuscript;
  * `reconstruct_PIV_fields.py`: This script plots the reconstructed instanteneous PIV velocity vectors from the POD results. 
