# Snapshot_POD
This folder contains our Python-based snapshot POD script. 
It reads files from the PIV_data folder and generate the POD decomposition fields in a .npz format.
  * `main.py`: Script that performs the POD decomposition;
