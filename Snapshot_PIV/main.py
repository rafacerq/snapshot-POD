
import matplotlib.pyplot as plt
import numpy as np
import os
from pathlib import Path
from scipy.signal import medfilt2d
import cv2
from numpy import linalg as LA
import sys

from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

import config as cfg
from return_folders import return_folders

"""
    That is the main script. It reads .npz transient PIV files and return a single .npz
    file with the POD reconstruction.
"""

# Debug option
debug = False

# Defining the experiment folder - Here you may pick any case from ../PIV_data
experiment = "PIV_300rpm_1.0BEP"

# Returning the results_folder
npy_folder = return_folders(experiment)

# Listing files from the numpy folder
_files = os.listdir(npy_folder)
_files.sort()
npy_files = []
for _file in _files:
    if _file.endswith(".npz"):
        npy_files.append(_file)

# Creating an array to store the timestamp array
time_global =[]
for npy_file in npy_files:
    dummy_file = np.load(os.path.join(npy_folder, npy_file))
    time_global.append(dummy_file['timestamp'])
time_global = np.asarray(time_global)


# MPI is used to parallelize the I/O. Therefore, in the folllowing lines, we'll
# start seeing "_global" values. Those variables are avaliable for all nodes(ranks)

# Now, we get the experiments encoder values
# NOTE: In the POD post-processing, there is no 'angle', because the
#       impeller is rotating frame-of-refence. Otherwise, it would not be
#       possible to do a POD analysis of the flow field

# Converting from a regular python list to numpy array
npy_files_global = np.asarray(npy_files)

# Now, It is necessary to generate a global numbering
N_PIV_files_global = len(npy_files_global)
indexes_global = np.arange(N_PIV_files_global)

# First, read a 'dummy' .npy file to get the correct dimensions
dummy_file = np.load(os.path.join(npy_folder, npy_files_global[0]))
X = dummy_file['X']
N_I = X.shape[0]; N_J = X.shape[1]

# Here I am dividing the array values by the number of ranks
# At this point, all ranks now about this partition
npy_files = np.array_split(npy_files_global, size)
indexes            = np.array_split(indexes_global  , size)

# It is important to know how many samples are in each rank
N_each_rank = np.zeros((size,), dtype=int)
for k in range(size):
    N_each_rank[k] = len(indexes[k])

# Here, I am waiting all the ranks to reach at this point.
# Then, they can execute the computations independtly
comm.Barrier()


# Now, each node(rank) is getting their portion of the array
# From now on, each node(rank) executes independtly
npy_files = npy_files[rank]
indexes   = indexes[rank]

# Let's start by getting the total number of files (on each rank!)
N_PIV_files = len(npy_files)

# Then, initialize the matrix
U_all_imp          = np.zeros((N_PIV_files, N_I, N_J), dtype=np.float64)
V_all_imp          = np.zeros((N_PIV_files, N_I, N_J), dtype=np.float64)
MASK_all_imp       = np.zeros((N_PIV_files, N_I, N_J), dtype=np.int32)
MASK_all_imp_geom  = np.zeros((N_PIV_files, N_I, N_J), dtype=np.int32)

U_all_imp[...] = 0.0
V_all_imp[...] = 0.0

# Now it is time to assemble the matrices
for i in range(N_PIV_files):

    print(f"Reading File rank {rank}, {i}/{N_PIV_files}")

    npy_file = np.load(os.path.join(npy_folder, npy_files[i]))
    X             = npy_file['X']
    Y             = npy_file['Y']
    U_imp         = npy_file['U_imp']
    V_imp         = npy_file['V_imp']
    MASK_imp      = npy_file['MASK_PIV_imp']
    center_mm     = npy_file['center_mm']
    MASK_imp_geom = npy_file['MASK_geom_imp']
    N_I = X.shape[0]; N_J = X.shape[1]

    # Updating matrices
    U_all_imp[i,:,:]         = U_imp
    V_all_imp[i,:,:]         = V_imp
    MASK_all_imp[i,:,:]      = MASK_imp

    # Apply masks to remove the impeller blades and outer(volute) geometry
    MASK_all_imp_geom[i,:,:] = MASK_imp_geom

# Let's wait all the processors fill their memory
comm.barrier()

# Now, we need to create the variables in rank 0 to receive them from
# the other ranks. Again, we'll adopt the '_global' nomenclature
# The "_tmp" nomenclature is used to send the variables
U_all_imp_tmp          = None
V_all_imp_tmp          = None
MASK_all_imp_tmp       = None
MASK_all_imp_geom_tmp  = None

# Waiting again to be sure...
comm.barrier()

# Scattering the data from all ranks to rank = 0
U_all_imp_tmp          = comm.gather(U_all_imp         , root=0)
V_all_imp_tmp          = comm.gather(V_all_imp         , root=0)
MASK_all_imp_tmp       = comm.gather(MASK_all_imp      , root=0)
MASK_all_imp_geom_tmp  = comm.gather(MASK_all_imp_geom , root=0)

# Let's wait all the processors fill their memory
comm.barrier()

# Now we need to actually send all the data to rank = 0
if rank == 0:
    # Here we are creating the large [N_data, N_I, N_J] matrix to store the data
    U_all_imp_global          = np.zeros((N_PIV_files_global, N_I, N_J), dtype=np.float64)
    V_all_imp_global          = np.zeros((N_PIV_files_global, N_I, N_J), dtype=np.float64)
    MASK_all_imp_global       = np.zeros((N_PIV_files_global, N_I, N_J), dtype=np.int32)
    MASK_all_imp_geom_global  = np.zeros((N_PIV_files_global, N_I, N_J), dtype=np.int32)

    # Sending the 'rank' data to the full matrix
    for k in range(0, size):
        k_1 = np.sum(N_each_rank[:k])
        k_2 = k_1 + N_each_rank[k]

        U_all_imp_global[k_1:k_2,:,:]         = U_all_imp_tmp[k]
        V_all_imp_global[k_1:k_2,:,:]         = V_all_imp_tmp[k]
        MASK_all_imp_global[k_1:k_2,:,:]      = MASK_all_imp_tmp[k]
        MASK_all_imp_geom_global[k_1:k_2,:,:] = MASK_all_imp_geom_tmp[k]

    # Now we have all the PIV data in rank 0 and we can do our
    # POD analysis in this rank.
    U_avg     = np.zeros((N_I, N_J), dtype=float)
    V_avg     = np.zeros((N_I, N_J), dtype=float)
    COUNT_avg = np.zeros((N_I, N_J), dtype=float)

    # So, let's loop over the PIV files (acquisitions)
    for k in range(N_PIV_files_global):
        # Retrieving the k-th acuqistion fom the large matrix
        U_imp         = U_all_imp_global[k,:,:]
        V_imp         = V_all_imp_global[k,:,:]
        MASK_imp      = MASK_all_imp_global[k, :, :]
        MASK_imp_geom = MASK_all_imp_geom_global[k, :, :]


        # Applying the impeller geometry mask
        U_imp[MASK_imp_geom > 10] = np.nan
        V_imp[MASK_imp_geom > 10] = np.nan

        #### DEBUG --- Check if the velocity values are OK
        if debug:
            ax = plt.gca()
            ax.set_aspect('equal','box')
            ax.axis('off')
            ax.set_xlim(80.0,190.0)
            ax.set_ylim(40.0,160.0)
            U_mag = np.sqrt(U_imp**2.0 + V_imp**2.0)
            quiv_1 = plt.quiver(X, Y, U_imp,V_imp, U_mag, units='xy',
                              scale_units=None, pivot='mid', scale=0.2)
            plt.show()

        COUNT_avg[MASK_imp_geom <= 10] += 1.0
        U_avg[MASK_imp_geom <= 10] += U_imp[MASK_imp_geom <= 10]
        V_avg[MASK_imp_geom <= 10] += V_imp[MASK_imp_geom <= 10]

    # Computing the average velocity field
    U_avg = U_avg / COUNT_avg
    V_avg = V_avg / COUNT_avg

    # Retrieving the PIV I and J dimensions
    N_x = U_avg.shape[0]
    N_y = U_avg.shape[1]

    # Creating a matrix to store the U and V fluct values
    U_fluc = np.zeros((N_PIV_files_global, N_x, N_y))
    V_fluc = np.zeros((N_PIV_files_global, N_x, N_y))
    for k in range(N_PIV_files_global):
        U_fluc[k,:,:] = U_all_imp_global[k,:,:] - U_avg
        V_fluc[k,:,:] = V_all_imp_global[k,:,:] - V_avg


    # Start of the snapshot-POD implementation.
    # The methodology follows what is described in
    # A Tutorial on the Proper Orthogonal Decomposition
    # Julien Weiss
    # doi.org/10.14279/depositonce-8512
    # June 2019

    # In the POD reconstruction we have the following:
    #
    #            N_x * N_y ---->                      modes --->       N_x * N_y ----->
    #  time    _                  _       time     _         _      _                     _   modes
    #    |    | ----------------   |        |     |   | | |   |    |   -----------------   |    |
    #    |    | ----------------   |  =     |     |   | | |   | .  |   -----------------   |    |
    #   \ /   |_----------------  _|       \ /    |_  | | |  _|    |_  -----------------  _|   \ /
    #    .                                  .                                                   .
    #               U(t)                               A         .        PHI^T

    # Creating a matrix to store the POD - U matrix
    POD_U = np.zeros((N_PIV_files_global, 2 * N_x * N_y), dtype=float)
    for k in range(N_PIV_files_global):
        U_ravel = np.ravel(U_fluc[k,:,:])
        V_ravel = np.ravel(V_fluc[k,:,:])
        U_ravel[np.isnan(U_ravel)] = 0.0
        V_ravel[np.isnan(V_ravel)] = 0.0
        POD_ravel = np.concatenate( (U_ravel, V_ravel))
        POD_U[k,:] = POD_ravel

    # Computing transpose of POD_U
    POD_U_T = np.transpose(POD_U)

    # Computing the covariance matrix
    COV = np.matmul(POD_U , POD_U_T) / (float(N_PIV_files_global) - 1.0)

    # Computing the eigenvalues and eigenvectors of the
    # U covariance matrix
    # LAM_s: eigenvalues
    # A_s: eigenvectors
    LAM_s, A_s = LA.eig(COV)

    # Remove imaginary part - NOTE: It may happen for small LAM_s values
    LAM_s = LAM_s.real
    A_s   = A_s.real

    # Sorting the A_s and LAM_s arrays from most to less energetic
    # Since here we are using the snapshot POD method,
    # the values of A_s represent the temporal modes
    order = np.argsort(LAM_s)
    order = order[::-1]
    A_s   = A_s[:,order]
    LAM_s = LAM_s[order]

    # Calculating the spatial coefficients
    PHI_s = np.matmul(POD_U_T, A_s)

    # Since we are using the Snaphsot-POD, it is necessary
    # to normalize the spatial coefficients to retrieve the
    # orignal POD method.
    PHI_norm = np.zeros_like(PHI_s)
    for k in range(N_PIV_files_global):
        column_squared = PHI_s[:,k]**2.0
        sum_column_squared = np.sum(column_squared)
        PHI_norm[:,k] = PHI_s[:,k] / np.sqrt(sum_column_squared)

    # Now we recover the temporal modes to match the standard POD
    # A: Time coefficients
    # PHI: Spatial modes
    PHI = PHI_norm
    A = np.matmul(POD_U, PHI)

    # Transpose of the PHI matrix, necessary for mode reconstruction
    PHI_T = np.transpose(PHI)

    # Reconstructing the different modes
    N_modes = LAM_s.shape[0]
    U_modes = np.zeros((N_modes, N_x, N_y), dtype=float)
    V_modes = np.zeros((N_modes, N_x, N_y), dtype=float)

    # This step resconstruct the original velocity (or fluc) vector field
    # But remember, you need to split and reararange each line to recover the
    # U and V velocity values
    U_reconstruct = np.matmul(A, PHI_T)

    # Storing POD decomposition into a .npz file
    # First, create the output folder
    np_out_folder = Path('POD_output')
    np_out_folder.mkdir(parents=True, exist_ok=True)

    # Then, store the file
    np_out_file = Path(np_out_folder,f"{experiment}_POD_reconstruct")
    np.savez_compressed(np_out_file,
                        X = X, Y = Y,
                        A = A, PHI_T = PHI_T,
                        LAM_s = LAM_s,
                        N_x = N_x, N_y = N_y,
                        U_avg = U_avg, V_avg = V_avg,
                        MASKS = MASK_all_imp_geom_global,
                        center_mm = center_mm)
