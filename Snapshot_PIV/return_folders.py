import os

import config as cfg


def return_folders(experiment):

    # Defining the root experiment folder
    root_experiment_folder = os.path.join(cfg.EXPERIMENTAL_FOLDER, experiment)

    # Defining the folder where the .npy files are stores
    npy_folder = os.path.join(root_experiment_folder)

    return npy_folder

